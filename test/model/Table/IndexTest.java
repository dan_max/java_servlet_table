/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Table;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author danmax
 */
public class IndexTest {
    
    public IndexTest() {
    }
    
    /**
     * Test of getX method, of class Index.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        Index instance = new Index(3,5);
        Object expResult = 3;
        Object result = instance.getX();
        assertEquals(expResult, result);
    }

    /**
     * Test of getY method, of class Index.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        Index instance = new Index(3,5);
        Object expResult = 5;
        Object result = instance.getY();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Index.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object o = new Index(5,5);
        Index instance = new Index(3,5);
        boolean expResult = false;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }
    
}
