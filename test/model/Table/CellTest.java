/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Table;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author danmax
 */
public class CellTest {
    
    public CellTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * Test of setContent method, of class Cell.
     */
    @Test
    public void testSetContent() {
        System.out.println("setContent");
        String s = "asd";
        Cell instance = new Cell(0,0);
        instance.setContent(s);
        assertEquals(s, instance.getContent());
    }

    /**
     * Test of getContent method, of class Cell.
     */
    @Test
    public void testGetContent() {
        System.out.println("getContent");
        String expResult = "asd";
        Cell instance = new Cell(0,0,expResult);
        String result = instance.getContent();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIndex method, of class Cell.
     */
    @Test
    public void testGetIndex() {
        System.out.println("getIndex");
        Cell instance = new Cell(3, 4, "qwe");
        Index expResult = new Index(3, 4);
        Index result = instance.getIndex();
        assertEquals(expResult, result);
    }
    
}
