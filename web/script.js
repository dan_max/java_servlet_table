/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function addr() { 
    var row = Number(document.getElementById("Y").innerHTML);
    var table = document.getElementById("mTab");
    var sizex = Number(document.getElementById("sizex").innerHTML);
    var new_tr = table.insertRow(row);
    var td;
    for(var i = 0; i < sizex; i++){
        td = document.createElement('td');
        td.innerHTML = " ";
        new_tr.appendChild(td);
    }
    document.getElementById("mTab").innerHTML = table.innerHTML;
    document.getElementById("sizey").innerHTML = Number(document.getElementById("sizey").innerHTML) + 1;
    document.getElementById("size").innerHTML = Number(document.getElementById("sizex").innerHTML) * Number(document.getElementById("sizey").innerHTML);
    save();
}

function addc() { 
    var col = Number(document.getElementById("X").innerHTML);
    var table = document.getElementById("mTab");
    var sizey = Number(document.getElementById("sizey").innerHTML);
    var new_td;
    for(var i = 0; i < sizey; i++){
        new_td = table.rows[i].insertCell(col);
        new_td.innerHTML = " "; 
    }
    document.getElementById("mTab").innerHTML = table.innerHTML;
    document.getElementById("sizex").innerHTML = Number(document.getElementById("sizex").innerHTML) + 1;
    document.getElementById("size").innerHTML = Number(document.getElementById("sizex").innerHTML) * Number(document.getElementById("sizey").innerHTML);
    save();
}

function delr() { 
    var row = Number(document.getElementById("Y").innerHTML);
    var table = document.getElementById("mTab");
    table.deleteRow(row);
    document.getElementById("mTab").innerHTML = table.innerHTML;
    document.getElementById("sizey").innerHTML = Number(document.getElementById("sizey").innerHTML) - 1;
    document.getElementById("size").innerHTML = Number(document.getElementById("sizex").innerHTML) * Number(document.getElementById("sizey").innerHTML);
    save();
}

function delc() { 
    var col = Number(document.getElementById("X").innerHTML);
    var table = document.getElementById("mTab");
    var sizey = Number(document.getElementById("sizey").innerHTML);
    for(var i = 0; i < sizey; i++){
        table.rows[i].deleteCell(col);
    }
    document.getElementById("mTab").innerHTML = table.innerHTML;
    document.getElementById("sizex").innerHTML = Number(document.getElementById("sizex").innerHTML) - 1;
    document.getElementById("size").innerHTML = Number(document.getElementById("sizex").innerHTML) * Number(document.getElementById("sizey").innerHTML);
    save();
}

window.addEventListener("load", function(){
	document.getElementById("mTab").addEventListener("dblclick", function(e){
		var elem = e.target || e.srcElement, field = document.createElement("textarea");
		field.value = elem.innerHTML.trim();
                document.getElementById("Y").innerHTML = elem.parentElement.rowIndex; 
                document.getElementById("X").innerHTML = elem.cellIndex;
		elem.innerHTML = "";
		elem.appendChild(field);    
		field.focus();
		field.addEventListener("blur",function(){
                    elem.innerHTML = this.value;
                    save();
		});
	});
});

function save(){
    var content = ""
    content += document.getElementById("name").innerHTML;
    content += "\n";
    content += document.getElementById("sizex").innerHTML;
    content += "\n";
    content += document.getElementById("sizey").innerHTML;
    content += "\n";
    content += document.getElementById("size").innerHTML;
    content += "\n";
    var table = document.getElementById("mTab");
    for(var i = 0; row = table.rows[i]; i++){
        for(var j = 0; col = row.cells[j]; j++){
            content += i;
            content += "\n";
            content += j;
            content += "\n";
            content += row.cells[j].innerHTML.trim();
            content += "\n";
        }
    }
    var body = "content=" + content;
    body = encodeURI(body);
    var xmlhttp = getXmlHttp();
    xmlhttp.open('POST', 'http://localhost:8084/table/Save', true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send(body);
}
function getXmlHttp() {
    var xmlhttp;
    try {
      xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
      xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

