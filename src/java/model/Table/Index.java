package model.Table;

/**
 *
 * @author danmax
 */
public class Index<X,Y> {
    private X x;
    private Y y;
    public Index(X x, Y y){
        this.x = x;
        this.y = y;
    }
    
    public X getX(){
        return x;
    }
    
    public Y getY(){
        return y;
    }
    
    @Override
    public boolean equals(Object o){
        if(!(o instanceof Index)){
            return false;
        }
        Index i = (Index) o;
        return (this.x.equals(i.getX()) && this.y.equals(i.getY()));
    }
}
