package model.Table;

/**
 *
 * @author danmax
 */
public class Cell {
    private Index index;
    private String content;
    public Cell(int x, int y){
        index = new Index(x,y);
    }
    public Cell(Index c, String s){
        index = c;
        content = s;
    }
    public Cell(int x, int y, String c){
        index = new Index(x,y);
        content = c;
    }
    public void setContent(String s){
        content = s;
    }
    public String getContent(){
        return content;
    }
    public Index getIndex(){
        return index;
    } 
}
