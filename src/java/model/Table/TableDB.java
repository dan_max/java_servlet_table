package model.Table;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author danmax
 */
public class TableDB {
    private ArrayList<Table> db;
    public TableDB(){
        try {
            db = new ArrayList<>();
            this.load();
        } catch (IOException ex) {}
    }
    public Table getTable(String name){
         for(Table t : db){
             if(t.getName().equals(name)){
                 return t;
             }
         }
         return null;
    }
    public void load() throws IOException{
        File folder = new File("//home/danmax/lab/java/java_servlet_table/DB/tables/");    
        for (final File fileEntry : folder.listFiles()) {
            int index = fileEntry.getName().indexOf(".");
            Table t = new Table(fileEntry.getName().substring(0,index));
            t.load();
            db.add(t);
        }
    }
    public void save() throws IOException{
        for(Table t : db){
            t.save();
        }
    }
    public ArrayList<String> getNames(){
        ArrayList<String> res = new ArrayList<>();
        for(Table t : db){
            res.add(t.getName());
        }
        return res;
    }
}
