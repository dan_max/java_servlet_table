package model.Table;

import java.io.IOException;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;
/**
 *
 * @author danmax
 */
public class Table {
    private String  name;
    private int sizex;
    private int sizey;
    private ArrayList<Cell> arr;
    public Table(String name){
        sizex = 0;
        sizey = 0;
        arr = new ArrayList<>();
        this.name = name;
    }

    public Table(int x, int y, String name){
        sizex = x;
        sizey = y;
        arr = new ArrayList<>();
        this.name = name;
    }
    
    public Cell getCell(int x, int y){
        for(Cell i : arr){
            if(i.getIndex().getX().equals(x) && 
                    i.getIndex().getY().equals(y)){
                return i;
            }
        }
        return null;
    }
    
    public String getName(){
        return name;
    }
    
    public void save() throws IOException{
        try{
            FileWriter writer = new FileWriter("//home/danmax/lab/java/java_servlet_table/DB/tables/" + name + ".txt");    
            writer.write(name);
            writer.append('\n');
            writer.write(String.valueOf(sizex));
            writer.append('\n');
            writer.write(String.valueOf(sizey));
            writer.append('\n');
            writer.write(String.valueOf(arr.size()));
            writer.append('\n');
            for(Cell i : arr){
                writer.write(i.getIndex().getX().toString());
                writer.append('\n');
                writer.write(i.getIndex().getY().toString());
                writer.append('\n');
                writer.write(i.getContent());
                writer.append('\n');
            }
            writer.close();
        }
        catch(IOException t){
            System.err.println(t);
        }
    }
    
    public void load() throws IOException{
        try(FileReader reader = new FileReader("//home/danmax/lab/java/java_servlet_table/DB/tables/" + name + ".txt"))
        {
            arr.clear();
            Scanner sc = new Scanner(reader);
            name = sc.nextLine();
            sizex = Integer.parseInt(sc.nextLine());
            sizey = Integer.parseInt(sc.nextLine());
            int x,y,cellcount;
            cellcount = Integer.parseInt(sc.nextLine());
            for(int i = 0; i < cellcount; ++i){
                x = Integer.parseInt(sc.nextLine());
                y = Integer.parseInt(sc.nextLine());
                arr.add(new Cell(x,y,sc.nextLine()));
            }
            reader.close();
        }
    }
    
    public int getX(){
        return sizex;
    }
    
    public int getY(){
        return sizey;
    }
    
    public void setCell(int x, int y, String s){
        for(Cell i : arr){
            if(i.getIndex().getX().equals(x) && i.getIndex().getY().equals(y)){
                i.setContent(s);
                break;
            }
        }
    }
    
    public void  initEmpty(){
        for(int i = 0; i < sizey; ++i){
            for(int j = 0; j < sizex; ++j){
                arr.add(new Cell(new Index(i, j), i + " " + j));
            }
        }
    }
}
