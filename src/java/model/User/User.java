package model.User;

/**
 *
 * @author danmax
 */
public class User {
    private final String name;
    private final String password;
    private final boolean editor;
    public User(String name, String password, boolean canEdit){
        this.name = name;
        this.password = password;
        this.editor = canEdit;
    }
    public String getName(){
        return name;
    }
    public String getPassword(){
        return password;
    }
    public boolean canEdit(){
        return editor;
    }
    @Override
    public boolean equals(Object o){
        if(!(o instanceof User)){
            return false;
        } 
        User u = (User) o;
        return this.name.equals(u.getName()) && this.password.equals(u.getPassword()) && !(this.editor ^ u.canEdit());
    }
}
