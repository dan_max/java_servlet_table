package model.User;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author danmax
 */
public class UserDB {
    private ArrayList<User> db;
    public UserDB() {
        try {
            this.db = new ArrayList<>();
            this.load();
        } catch (IOException ex) {}
    }
    public void add(User u){
        db.add(u);
    }
    public boolean hasName(User u){
        for(User i : db){
            if(i.getName().equals(u.getName())){
                return true;
            }
        }
        return false;
    }
    public boolean hasName(String name){
        for(User i : db){
            if(i.getName().equals(name)){
                return true;
            }
        }
        return false;
    }
    public User getUser(String name){
        for(User i : db){
            if(i.getName().equals(name)){
                return i;
            }
        }
        return null;
    }
    
    public void save() throws IOException{
        try(FileWriter writer = new FileWriter("//home/danmax/lab/java/java_servlet_table/DB/usersDB.txt")){
            writer.write(Integer.toString(db.size()));
            writer.append('\n');
            for(User i : db){
                writer.write(i.getName());
                writer.append('\n');
                writer.write(i.getPassword());
                writer.append('\n');
                if(i.canEdit()){
                    writer.write("true");
                }
                else{
                    writer.write("false");
                }
                writer.append('\n');
            }
            writer.close();
        }
    }
    public void load() throws IOException{
        try(FileReader reader = new FileReader("//home/danmax/lab/java/java_servlet_table/DB/usersDB.txt")){
            db.clear();
            Scanner sc = new Scanner(reader);
            int count = Integer.parseInt(sc.nextLine());
            String name, password;
            boolean canEdit;
            for(int i = 0; i < count; ++i){
                name = sc.nextLine();
                password = sc.nextLine();
                canEdit = Boolean.parseBoolean(sc.nextLine());
                db.add(new User(name, password,canEdit));
            }
            reader.close();
        }
    }
}
