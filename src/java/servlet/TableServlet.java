package servlet;

import model.Table.Table;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Table.TableDB;

/**
 *
 * @author danmax
 */
public class TableServlet extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Cookie[] cookies = request.getCookies();
            String username = "";
            boolean edit = false;
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("name")) {
                        username = cookie.getValue();
                    }
                    if (cookie.getName().equals("type")){
                        edit = Boolean.parseBoolean(cookie.getValue());
                    }
                }
            }
            if(username.equals("")){
                response.sendRedirect("http://localhost:8084/table/Login");
            }
            String exit = request.getParameter("exit");
            if(exit != null){
                if(exit.equals("on")){
                    if (cookies != null){
                        for (Cookie cookie : cookies) {
                            cookie.setValue("");
                            cookie.setPath("/");
                            cookie.setMaxAge(0);
                            response.addCookie(cookie);
                        }
                    }
                    response.sendRedirect("http://localhost:8084/table/Login");
                    return;      
                }
            }
            TableDB db = new TableDB();
            String name = request.getParameter("name");
            Table t = null;
            if(name != null){
                t = db.getTable(name);
            }
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>" + t.getName() + "</title>");
            out.println("<link rel=stylesheet href=table.css>");
            if(edit){
                out.println("<script src =\"script.js\"></script>");
            }
            out.println("</head>");
            out.println("<body>");
            out.println("<div align=\"right\">User: " + username + 
                    "<form><button type=\"submit\" name=\"exit\" value=\"on\">Exit</button></form></div>");
            if(edit){
                out.println("<header>");
                out.println("<p id=Y hidden>0</p>");
                out.println("<p id=X hidden>0</p>");
                out.println("<button onclick=\"addr()\">Add row</button>");
                out.println("<button onclick=\"delr()\">Delete row</button>");
                out.println("<button onclick=\"addc()\">Add collumn</button>");
                out.println("<button onclick=\"delc()\">Delete collumn</button>");
                out.println("</header>");
                
            }  
            out.println("<form id=\"Ftab\">");
            out.println("<p id=\"name\">" + t.getName() + "</p>");
            out.println("<p id=\"sizex\" hidden>" + t.getX() + "</p>");
            out.println("<p id=\"sizey\" hidden>" + t.getY() + "</p>");
            out.println("<p id=\"size\" hidden>" + t.getX() * t.getY() + "</p>");
            out.println("<table id=\"mTab\">");
            for(int i = 0; i < t.getY(); ++i){
                out.println("<tr>");
                for(int j = 0; j < t.getX(); ++j){
                    out.println("<td>");
                    out.println(t.getCell(i, j).getContent());
                    out.println("</td>");
                }
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
