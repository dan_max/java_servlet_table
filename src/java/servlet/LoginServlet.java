package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User.User;
import model.User.UserDB;

/**
 *
 * @author danmax
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String name = request.getParameter("username");
            String pass = request.getParameter("pass");
            boolean alert = false;
            String alertmsg = "";
            if(name != null && pass != null){
                UserDB db = new UserDB();
                User u = db.getUser(Integer.toString(name.hashCode()));
                if(u != null){
                    if(u.getPassword().equals(Integer.toString(pass.hashCode()))){
                        Cookie c;
                        if(u.canEdit()){
                            c = new Cookie("type", "true");
                        }
                        else{
                            c = new Cookie("type", "false");
                        }
                        response.addCookie(c);
                        response.addCookie(new Cookie("name",name));
                        response.sendRedirect("http://localhost:8084/table/List");
                    }
                    else{
                        alert = true;
                        alertmsg = "Wrong password";
                    }
                }
                else{
                    alert = true;
                    alertmsg = "User not found";
                }
            }
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Login Page</title>");
            out.println("<link rel=stylesheet href=style.css>");
            if(alert){
                out.println("<script>");
                out.println("alert(\"" + alertmsg + "\");");
                out.println("</script>");
            }
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<form id=login>");
            out.println("<p>Username</p>");
            out.println("<p> <input type=\"text\" name=\"username\" required> </p>");
            out.println("<p>Password</p>");
            out.println("<p> <input type=\"password\" name=\"pass\" required> </p>");
            out.println("<button type=\"submit\">Sign in</button>");
            out.println("</form>");
            out.println("<button type=\"submit\" onClick=\"location.href='http://localhost:8084/table/Register'\">Register</button>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
