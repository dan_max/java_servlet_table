package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User.User;
import model.User.UserDB;

/**
 *
 * @author danmax
 */
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String name = request.getParameter("username");
            String pass = request.getParameter("pass");
            String type = request.getParameter("type");
            boolean alert = false;
            String t_pass_curr = "teacher";
            String alertmsg = "";
            if(name != null && pass != null && type != null){
                if(!name.equals("") && !pass.equals("")){
                    UserDB db = new UserDB();
                    if(!db.hasName(Integer.toString(name.hashCode()))){
                        if(type.equals("teacher")){                         
                            String t_pass = request.getParameter("pass_teacher");
                            if(t_pass != null && t_pass.equals(t_pass_curr)){
                                User u = new User(Integer.toString(name.hashCode()), Integer.toString(pass.hashCode()), true);
                                db.add(u);
                                db.save();
                                response.addCookie(new Cookie("type", "true"));
                                response.addCookie(new Cookie("name",name));
                                response.sendRedirect("http://localhost:8084/table/List");
                            }
                            else{
                                alert = true;
                                alertmsg = "Wrong teacher password";
                            }
                        }
                        else if(type.equals("student")){
                            User u = new User(Integer.toString(name.hashCode()), Integer.toString(pass.hashCode()), false);
                            db.add(u);
                            response.addCookie(new Cookie("type", "false"));
                            db.save();
                            response.addCookie(new Cookie("name",name));
                            response.sendRedirect("http://localhost:8084/table/List");
                        }
                    }
                    else{
                        alert = true;
                        alertmsg = "User alredy exist";
                    }
                }
                else{
                    alert = true;
                    alertmsg = "Input alInputl fields";
                }
            }
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Registration</title>");
            out.println("<link rel=stylesheet href=style.css>");
            out.println("<script src =\"register.js\"></script>");
            if(alert){
                out.println("<script>");
                out.println("alert(\"" + alertmsg + "\");");
                out.println("</script>");
            }
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<form id=reg>");
            out.println("<p>Username</p>");
            out.println("<p> <input type=\"text\" name=\"username\" required> </p>");
            out.println("<p>Password</p>");
            out.println("<p> <input type=\"password\" name=\"pass\" required> </p>");
            out.println("<p><select name=\"type\" id =\"type\" onchange=\"check()\"> </p>");
            out.println("<option disabled>User type</option>");
            out.println("<option value=\"student\">Student</option>");
            out.println("<option value=\"teacher\">Teacher</option>");
            out.println("</select>");
            out.println("<p id = \"p_teacher\" hidden = \"true\">Password for teacher</p>");
            out.println("<p> <input id = \"pass_teacher\" type=\"password\" name=\"pass_teacher\" hidden = \"true\"> </p>");
            out.println("<p><button type=\"submit\">Register</button></p>");
            out.println("</form>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
